package me.martl.foodtinder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Mart on 12.03.2015.
 */
public class RateActivity extends ActionBarActivity {

    public ParseObject currentObject;
    public List<String> likedPics = new ArrayList<String>();
    public static List<String> disLikedPics = new ArrayList<String>();
    private ImageView preview;
    private String objectId = null;
    private int count = 0;
    public int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);
        preview = (ImageView) findViewById(R.id.preview);

        Button b = (Button) findViewById(R.id.dislike);
        if(count == size )  b.setEnabled(false);
        checkLiked();
        showImage();

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Pause", "paused");
        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }

    public void checkLiked() {


        ParseQuery<ParseObject> query2 = ParseQuery.getQuery("Like");
        query2.whereEqualTo("user", ParseUser.getCurrentUser());
        query2.include("Image");
        query2.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> results, ParseException e) {
                //ParseObject pointerObj = new ParseObject("Like");


                //Toast.makeText(RateActivity.this, "Vastused likedes: " + results.size(), Toast.LENGTH_SHORT).show();
                for (ParseObject pointerObj : results) {
                    //pointerObj = results.get(0);
                    ParseObject obj = pointerObj.getParseObject("Image");
                    String name = obj.getObjectId();
                    if (!likedPics.contains(name)) likedPics.add(name);


                }
                //Toast.makeText(RateActivity.this, "liketud pilte: " + likedPics.size(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void showImage() {
        preview = (ImageView) findViewById(R.id.preview);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Image");
        query.whereNotEqualTo("user", ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {

                //Toast.makeText(RateActivity.this, "Vastused: " + parseObjects.size(), Toast.LENGTH_SHORT).show();
                if (count != parseObjects.size()) {
                    final ParseObject firstObject = parseObjects.get(count);
                    size = parseObjects.size();
                    currentObject = firstObject;
                    ParseFile imgFile = (ParseFile) firstObject.get("imageFile");
                    objectId = firstObject.getObjectId();
                    if (likedPics.contains(objectId) || disLikedPics.contains(objectId)) {
                        //Toast.makeText(RateActivity.this, "contains! ", Toast.LENGTH_SHORT).show();
                        count++;
                        showImage();
                    } else {
                        //Toast.makeText(RateActivity.this, "count: " + count, Toast.LENGTH_SHORT).show();
                        imgFile.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] bytes, ParseException e) {
                                if (e == null) {
                                    //käes

                                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                                    preview.setImageBitmap(bitmap);
                                    Button b = (Button) findViewById(R.id.dislike);
                                    b.setEnabled(true);


                                } else {
                                    //not käes
                                }
                            }
                        });
                    }


                } else {
                    Toast.makeText(RateActivity.this, "Pidu läbi, kõik läbi vaadatud ", Toast.LENGTH_SHORT).show();
                    preview.setImageResource(R.drawable.ic_launcher);
                }

            }
        });
    }

    public void like(View v) {
        ParseObject like = new ParseObject("Like");
        like.put("user", ParseUser.getCurrentUser());
        like.put("Image", currentObject);

        // Toast.makeText(RateActivity.this, "laikisid: " + objectId, Toast.LENGTH_SHORT).show();
        like.saveInBackground();

        Intent i = new Intent(this, LikedActivity.class);
        i.putExtra("myPhoto", currentObject.getObjectId());
        i.putExtra("likeId", like.getObjectId());
        i.putExtra("delete", 2);
        count++;
        showImage();
        startActivity(i);
    }

    public void disLike(View v) {
        Button b = (Button) findViewById(R.id.dislike);
        Log.d("dislike", String.valueOf(size));
        Log.d("dislike", String.valueOf(count));

        if(count == size )  b.setEnabled(false);

        try {
            disLikedPics.add(currentObject.getObjectId());
            count++;
            if(count< size)  showImage();

        } catch (Exception e) {
            Button cc = (Button) findViewById(R.id.likebutton);
            cc.setEnabled(false);
            b.setEnabled(false);
            Toast.makeText(RateActivity.this, "Pidu läbi, kõik läbi vaadatud ", Toast.LENGTH_SHORT).show();

            preview.setImageResource(R.drawable.ic_launcher);
        }

    }

    public void showRateView(View v) {
        Intent i = new Intent(this, RateActivity.class);
        startActivity(i);
        finish();
    }

    public void showMatchesView(View v) {

        Intent i = new Intent(this, MatchesActivity.class);
        startActivity(i);
        finish();
    }

    public void showProfileView(View v) {

        Intent i = new Intent(this, ProfileActivity.class);
        startActivity(i);
        finish();
    }
}
