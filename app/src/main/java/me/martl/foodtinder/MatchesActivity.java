package me.martl.foodtinder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mart on 17.03.2015.
 */
public class MatchesActivity extends ActionBarActivity {

    public List<MyPhoto> likedObjects = new ArrayList<MyPhoto>();
    //  public String likeId;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Like");
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        query.include("Image");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> results, ParseException e) {


                // Toast.makeText(RateActivity.this, "Vastused likedes: " +  results.size(), Toast.LENGTH_SHORT).show();
                for (final ParseObject pointerObj : results) { //loobib liked läbi
                    // likeId = pointerObj.getObjectId();
                    //Log.d("likeId", likeId);
                    final ParseObject obj = (ParseObject) pointerObj.getParseObject("Image");
                    ParseFile imgFile = (ParseFile) obj.get("imageFile");
                    final String info = obj.getObjectId();

                    imgFile.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] bytes, ParseException e) {
                            if (e == null) {

                                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                //Toast.makeText(MatchesActivity.this, "pilt: "  + obj.getObjectId() , Toast.LENGTH_SHORT).show();
                                likedObjects.add(new MyPhoto(bitmap, info, pointerObj.getObjectId()));
                                //Log.d("TAG", info);
                                populateListView();


                            }
                        }
                    });


                }

            }
        });

        registerClickCallback();


    }

    @Override
    protected void onPause() {
        super.onPause();
        //Log.d("Pause", "paused");
        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }

    private void registerClickCallback() {
        ListView list = (ListView) findViewById(R.id.picsListView);
        final Intent i = new Intent(this, PhotoDetailActivity.class);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyPhoto clickedPhoto = likedObjects.get(position);
                Log.d("klikk, likeid", clickedPhoto.getLikeId());
                i.putExtra("myPhoto", clickedPhoto.getInfo());
                i.putExtra("likeId", clickedPhoto.getLikeId());

                startActivity(i);
            }
        });
    }

    private void populateListView() {
        ArrayAdapter<MyPhoto> adapter = new MyListAdapter();
        //Toast.makeText(MatchesActivity.this, "populate "  , Toast.LENGTH_SHORT).show();
        //Log.d("TAG", "populate");
        ListView list = (ListView) findViewById(R.id.picsListView);
        list.setAdapter(adapter);
    }

    public void showRateView(View v) {
        Intent i = new Intent(this, RateActivity.class);
        startActivity(i);
        finish();
    }

    public void showMatchesView(View v) {
        Intent i = new Intent(this, MatchesActivity.class);
        startActivity(i);
        finish();
    }

    public void showProfileView(View v) {

        Intent i = new Intent(this, ProfileActivity.class);
        startActivity(i);
        finish();
    }

    private class MyListAdapter extends ArrayAdapter<MyPhoto> {
        public MyListAdapter() {

            super(MatchesActivity.this, R.layout.item_view, likedObjects);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) { //position mitmes element listis

            //Toast.makeText(MatchesActivity.this, "override "  , Toast.LENGTH_SHORT).show();
            View itemView = convertView;
            if (itemView == null) {
                itemView = getLayoutInflater().inflate(R.layout.item_view, parent, false);
            }

            MyPhoto currentPhoto = likedObjects.get(position);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.item_icon);
            imageView.setImageBitmap(currentPhoto.getBitmap());

            TextView textView = (TextView) itemView.findViewById(R.id.item_info);
            textView.setText(currentPhoto.getInfo());

            return itemView;
        }
    }
}
