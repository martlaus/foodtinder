package me.martl.foodtinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by Mart on 17.03.2015.
 */
public class LikedActivity extends ActionBarActivity {
    private String currentObjId;
    public int delete;
    public String likeId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liked);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            currentObjId = extras.getString("myPhoto");
            likeId = extras.getString("likeId");
            delete = extras.getInt("delete");
            //Toast.makeText(PhotoDetailActivity.this, "delete " + extras.getInt("delete"), Toast.LENGTH_SHORT).show();
        }
    }

    public void continueLiking(View v) {
        this.finish();
    }

    public void info(View v) {

        final Intent i = new Intent(this, PhotoDetailActivity.class);


               // Log.d("klikk, likeid", clickedPhoto.getLikeId());
                i.putExtra("myPhoto", currentObjId);
                i.putExtra("likeId", likeId);
                i.putExtra("delete", delete);

                startActivity(i);

    }
}
