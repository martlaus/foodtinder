package me.martl.foodtinder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Mart on 18.03.2015.
 */
public class PhotoDetailActivity extends ActionBarActivity {
    public String likeId;
    private ImageView preview;
    private String currentObjId;
    public int delete;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        preview = (ImageView) findViewById(R.id.preview);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            currentObjId = extras.getString("myPhoto");
            likeId = extras.getString("likeId");
            delete = extras.getInt("delete");
            //Toast.makeText(PhotoDetailActivity.this, "delete " + extras.getInt("delete"), Toast.LENGTH_SHORT).show();
        }
        TextView textView = (TextView) findViewById(R.id.item_info);
        textView.setText(currentObjId);
        preview = (ImageView) findViewById(R.id.preview);



        ParseQuery<ParseObject> query = ParseQuery.getQuery("Image");
        query.whereEqualTo("objectId", currentObjId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {

                final ParseObject firstObject = parseObjects.get(0);
                String desc = firstObject.getString("userInput");
                TextView description = (TextView) findViewById(R.id.item_description);
                description.setText(desc);

                ParseFile imgFile = (ParseFile) firstObject.get("imageFile");

                imgFile.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        if (e == null) {
                            //käes

                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                            preview.setImageBitmap(bitmap);


                        }
                    }
                });


            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Log.d("Pause", "paused");
        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }

    public void delete(View v) {

        Button b = (Button) findViewById(R.id.delete);
        //Log.d("asd", "trying to delete");
        //Log.d("asd", String.valueOf(likeId));
        Log.d("NR", String.valueOf(delete));
        if(delete == 1){// delete my pic
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Image");
            query.whereEqualTo("objectId", currentObjId);

            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> results, ParseException e) {
                   // Log.d("kustutavate suurus", String.valueOf(results.size()) + " " + results.get(0).getObjectId());
                    ParseObject p = results.get(0);
                    p.deleteInBackground();

                }
            });
        }
        else if(delete == 2 ){
            Toast.makeText(PhotoDetailActivity.this, "Et pilti kustutada pead teda ennem like-ma või ise lisama", Toast.LENGTH_SHORT).show();
        }
        else{//delete from likes
            try{
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Like");
            query.whereEqualTo("objectId", likeId);

            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> results, ParseException e) {
                    Log.d("asd", String.valueOf(results.size()));
                    ParseObject p = results.get(0);
                    if (p.deleteInBackground().isCompleted()) Log.d("asd", "deleted");

                }
            });
        }catch (Exception e){

            }
        }
        b.setEnabled(false);
    }

    public void showRateView(View v) {
        Intent i = new Intent(this, RateActivity.class);
        startActivity(i);
    }

    public void showMatchesView(View v) {
        Intent i = new Intent(this, MatchesActivity.class);
        startActivity(i);
    }

    public void showProfileView(View v) {

        Intent i = new Intent(this, ProfileActivity.class);
        startActivity(i);
    }
}
