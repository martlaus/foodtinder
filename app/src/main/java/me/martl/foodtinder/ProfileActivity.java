package me.martl.foodtinder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mart on 18.03.2015.
 */
public class ProfileActivity extends ActionBarActivity {
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    public List<MyPhoto> likedObjects = new ArrayList<MyPhoto>();
    GridView imgGrid;
    public String userInput;
    public String newObjId;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        imgGrid = (GridView) findViewById(R.id.gridView);

        TextView username = (TextView) findViewById(R.id.userName);
        ParseUser user = ParseUser.getCurrentUser();

        username.setText(user.getUsername());

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Image");
        query.whereEqualTo("user", ParseUser.getCurrentUser());

        final GridAdapter g = new GridAdapter(this);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> results, ParseException e) {

                //Toast.makeText(ProfileActivity.this, "Vastused likedes: " +  results.size(), Toast.LENGTH_SHORT).show();
                for (ParseObject pointerObj : results) { //loobib liked läbi
                    ParseFile imgFile = (ParseFile) pointerObj.get("imageFile");
                    final String info = pointerObj.getObjectId();

                    imgFile.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] bytes, ParseException e) {
                            if (e == null) {
                                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                likedObjects.add(new MyPhoto(bitmap, info, info));
                                imgGrid.setAdapter(g);
                            }
                        }
                    });
                }
            }
        });
        registerClickCallback();
    }

    public void changeName(View view){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Uus kasutajanimi:");
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                ParseUser currentUser = ParseUser.getCurrentUser();
                currentUser.setUsername(input.getText().toString());
                currentUser.saveInBackground();

                finish();
                startActivity(getIntent());
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
                startActivity(getIntent());
            }
        });

        alert.show();




    }

    private void registerClickCallback() {
        imgGrid = (GridView) findViewById(R.id.gridView);

        final Intent i = new Intent(this, PhotoDetailActivity.class);

        imgGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyPhoto clickedPhoto = likedObjects.get(position);
                Log.d("klikk, id:", clickedPhoto.getInfo());
                i.putExtra("myPhoto", clickedPhoto.getInfo());
                i.putExtra("likeId", clickedPhoto.getLikeId());
                i.putExtra("delete", 1);

                startActivity(i);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Log.d("Pause", "paused");
        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }

    public void chooseMyPicture(View view) {
        final String[] items = {"take photo", "choose from lib", "cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("add phtoto!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (items[which]) {
                    case "take photo":
                        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "temp.png");
                        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile((file)));
                        startActivityForResult(takePhotoIntent, REQUEST_CAMERA);
                        break;
                    case "choose from lib":
                        Intent choosePhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        choosePhotoIntent.setType("image/*");
                        startActivityForResult(Intent.createChooser(choosePhotoIntent, "Select file"), SELECT_FILE);

                        break;
                    default:
                }

            }
        });

        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ImageView preview = (ImageView) findViewById(R.id.preview);
        //System.out.println("Damn son whered you find this");

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/temp.png");

                Bitmap image = BitmapFactory.decodeFile(file.getAbsolutePath());
               // Log.d("ASD", "tegi pilti");
                //preview.setImageBitmap(image);

                //kogu bytearrayks tegemise ja pildina salvestamise asi:
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 60, stream);//100 orig
                byte[] byteArray = stream.toByteArray();
                ParseFile imageFile = new ParseFile("temp.png", byteArray);
                imageFile.saveInBackground();

                //lisan pildi kohta info:
                final ParseObject addImg = new ParseObject("Image");
                addImg.put("user", ParseUser.getCurrentUser());
                addImg.put("imageFile", imageFile);

                addImg.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e == null) {
                            //Log.d("callback", addImg.getObjectId());
                            newObjId = addImg.getObjectId();
                            askLocation();
                        }
                    }
                });

                file.delete();
            } else if (requestCode == SELECT_FILE) {
                Uri selectImageUri = data.getData();
                String tempImagePath = getRealPathFromUri(selectImageUri);
                Bitmap image = BitmapFactory.decodeFile(tempImagePath);
                preview.setImageBitmap(image);

//                //kogu bytearrayks tegemise ja pildina salvestamise asi:
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                image.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                byte[] byteArray = stream.toByteArray();
//                ParseFile imageFile = new ParseFile("temp.png", byteArray);
//                imageFile.saveInBackground();
//
//                //lisan pildi kohta info:
//                ParseObject addImg = new ParseObject("Image");
//                addImg.put("user", ParseUser.getCurrentUser());
//                addImg.put("imageFile", imageFile);
//
//                addImg.saveInBackground();
            }
        }
    }

    private String getRealPathFromUri(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {

            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public void askLocation(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Lisa info");


        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                userInput = input.getText().toString();
                Log.d("location", userInput  + " " + newObjId);

                ParseObject newImg = ParseObject.createWithoutData("Image", newObjId);
                newImg.put("userInput", userInput);
                newImg.saveInBackground();

                finish();
                startActivity(getIntent());
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.

                finish();
                startActivity(getIntent());
            }
        });

        alert.show();
    }

    public void logOut(View view){
        ParseUser.logOut();

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    public void showProfileView(View v) {

        Intent i = new Intent(this, ProfileActivity.class);
        startActivity(i);
        finish();
    }

    public void showRateView(View v) {
        Intent i = new Intent(this, RateActivity.class);
        startActivity(i);
        finish();
    }

    public void showMatchesView(View v) {
        Intent i = new Intent(this, MatchesActivity.class);
        startActivity(i);
        finish();
    }

    public class GridAdapter extends BaseAdapter {
        Context context;

        GridAdapter(Context context) {

            this.context = context;
            List list = likedObjects;
        }

        @Override
        public int getCount() {
            // Log.d("TAG getcount", String.valueOf(likedObjects.size()));
            return likedObjects.size();
        }

        @Override
        public Object getItem(int position) {
            // Log.d("TAG getitem", String.valueOf(position));
            return likedObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            // Log.d("TAG2 getitem", String.valueOf(position));
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //Toast.makeText(ProfileActivity.this, "getview " , Toast.LENGTH_SHORT).show();
            View row = convertView;
            ViewHolder holder = null;

            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.grid_item, parent, false);
                holder = new ViewHolder(row);
                row.setTag(holder);
            } else holder = (ViewHolder) row.getTag();

            Bitmap b = likedObjects.get(position).getBitmap();
            holder.myPicture.setImageBitmap(b);

//            ParseImageView imageView = null;
//
//            imageView.setParseFile(null);
//            imageView.loadInBackground();

            return row;
        }

        public class ViewHolder {
            ImageView myPicture;

            ViewHolder(View v) {
                myPicture = (ImageView) v.findViewById(R.id.imageView);
            }
        }
    }
}
