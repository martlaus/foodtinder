package me.martl.foodtinder;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;


public class fotoActivity extends ActionBarActivity {

    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;
    private ImageView preview;
    private Button rateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);
        preview = (ImageView) findViewById(R.id.preview);

    }

    public void showRateView(View v) {
        Intent i = new Intent(this, RateActivity.class);
        startActivity(i);
    }

    public void showMatchesView(View v) {
        // Toast.makeText(fotoActivity.this, "showmatches", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, MatchesActivity.class);
        startActivity(i);
    }

    public void chooseImage(View view) {
        final String[] items = {"take photo", "choose from lib", "cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("add phtoto!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (items[which]) {
                    case "take photo":
                        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "temp.png");
                        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile((file)));
                        startActivityForResult(takePhotoIntent, REQUEST_CAMERA);
                        break;
                    case "choose from lib":
                        Intent choosePhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        choosePhotoIntent.setType("image/*");
                        startActivityForResult(Intent.createChooser(choosePhotoIntent, "Select file"), SELECT_FILE);

                        break;
                    default:
                }

            }
        });

        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //System.out.println("Damn son whered you find this");

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile().toString() + "/temp.png");
                Bitmap image = BitmapFactory.decodeFile(file.getAbsolutePath());
                preview.setImageBitmap(image);
                Log.d("ASD", "tegi pilti");
                file.delete();
            } else if (requestCode == SELECT_FILE) {
                Uri selectImageUri = data.getData();
                String tempImagePath = getRealPathFromUri(selectImageUri);
                Log.d("ASD", tempImagePath);
                Bitmap image = BitmapFactory.decodeFile(tempImagePath);
                preview.setImageBitmap(image);
            }
        }
    }

    private String getRealPathFromUri(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
}
