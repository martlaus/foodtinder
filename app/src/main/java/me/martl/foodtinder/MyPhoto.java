package me.martl.foodtinder;

import android.graphics.Bitmap;

/**
 * Created by Mart on 18.03.2015.
 */
public class MyPhoto {
    Bitmap bitmap;
    String info;
    String likeId;


    MyPhoto(Bitmap bitmap, String info, String likeId) {
        this.bitmap = bitmap;
        this.info = info;
        this.likeId = likeId;

    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getInfo() {
        return info;
    }

    public String getLikeId() {
        return likeId;
    }
}
